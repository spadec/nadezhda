import './css/owl.carousel.min.css';
import './css/styles.css';
import './css/slick.css';
import './sass/main.sass';
import $ from 'jquery';
window.jQuery = $;
window.$ = $;
require('owl.carousel')($);
require('slick-carousel')($);
require('jquery-mask-plugin')($);
require('./js/circle.js')($);
require('./js/actions.js')($);
import 'popper.js';
import 'bootstrap';

