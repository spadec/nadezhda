
module.exports = function($){
    /**
     * Действие при загрузки страницы
     */
    $(document).ready(function(){
        $("#phone").mask('+7(000) 000-00-00', {placeholder: "Телефон"});
        $(".joomly-callback-field").mask('+7(000) 000-00-00', {placeholder: "+7(777)777-77-77"});
        $('#spoiler').click(function(){
          $('.spoiler').slideToggle(500);
          $(this).text($(this).text() == 'Посмотреть все' ? 'Скрыть' : 'Посмотреть все');
       });
        /**
         * слайдер на главной странице
         */
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            items:1,
            loop:true,
            margin: 0,
            autoplay:true,
            autoplayTimeout:6000,
            autoplayHoverPause:true,
            center: true,
            dots: false,
            stagePadding: 0,
            autoWidth: true
        });
      });
    /*
    * Клик Раскрыть мобильное меню
    */
    $("#tooglerdata").click(function(){
        $(this).hide();
        $("#closeButton").show();
        $("#mobileMenu").show();
    });
    /**
     * Клик закрыть мобильное меню
     */
    $("#closeButton").click(function(){
        $(this).hide();
        $("#tooglerdata").show();
        $("#mobileMenu").hide();
    });
    /**
     * Событие hover по пункту осносного меню
     */
    $(document).on("mouseenter", "#rowLogoAndMenu .nav-item", function(e) {
      //$( this ).removeClass('active');
      var already = this.className;
      if(already.indexOf('active') < 0){
        $( this ).append( $( "<span id='yellow'> </span>" ) );
      }
    });
    $(document).on("mouseleave", "#rowLogoAndMenu .nav-item", function(e) {
      $( this ).find( "span" ).last().remove();
    });
    $(document).on("mouseenter", ".subnavbar-nav li", function(e) {
      var already = this.className;
      if(already.indexOf('active') < 0){
        $( this ).append( $( "<span id='yellow2'> </span>" ) );
      }
    });
    $(document).on("mouseleave", ".subnavbar-nav li", function(e) {
      $( this ).find( "span" ).last().remove();
    });
    /**
     * Закрывает подменю основного меню
     */
    $("#closeSubIcon").click(function(){
        $("#submenu").removeClass('d-lg-block');
        $("#submenu").hide();
    });

    /**
    * Hover эффект на блоках под слайдером
    * */
    $(".b01").hover(
      function(){
          $( this ).removeClass('b01');
          $( this ).addClass('b01Hover');
      }, function() {
          $( this ).removeClass('b01Hover');
          $( this ).addClass('b01');    
      }
    );
    /**
     * Показывает меню подпункта когда этот пункт активирован
     */
    var list = $("#mainmenu li.active").find('ul li');
    if(list.length>0){
        $("#submenu").addClass('d-lg-block')
       for (let i = 0; i < list.length; i++) {
           $(list[i]).removeClass('nav-item').addClass('subnav-item');
           $(list[i]).find('a').removeClass('nav-link ');
           $(".subnavbar-nav").append(list[i]);
       }
    }
    /**
     * показывает модальное окно когда отправленно сообщение
     */
    if($('#myModal').length > 0){
        $('#myModal').modal('show');
    }
    /**
     * Карусель модуля новости на главной 
     */
    $('.multiple-items').slick({ 
        slidesToShow: 3,
        centerMode: true,
        centerPadding: '0px',
        arrows: false,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        responsive: [
            {
              breakpoint: 1000,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            // You can unslick at a given breakpoint now by adding:
            // settings: "unslick"
            // instead of a settings object
          ]
    });
    /**
     * Карусель модуля отзывы
     */
    $('.testi-items').slick({
        slidesToShow: 3,
        infinite: true,
        centerPadding: '0px',
        centerMode: true,
        arrows: false,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 4000,
        focusOnSelect: true,
        responsive: [
            {
              breakpoint: 1000,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
              }
            },
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
    });
    $('.test-info-0').show();
    $('.testi-img-0').addClass('testImg');
    $('.testi-items').on('afterChange', function(currentSlide, nextSlide){
      var currentSlide = $('.testi-items').slick('slickCurrentSlide');
      var prevSlide = 0, mynextSlide = 0,
      fulllength = $('.testi-all').length - Number(1);
      if(currentSlide > 0){
        prevSlide = Number(currentSlide) - Number(1);
        mynextSlide = Number(currentSlide) + Number(1);
        if(mynextSlide > fulllength){
          mynextSlide = 0;
        }
      }
      if(currentSlide == 0){
        $(endselector).hide();
        $('.test-info-0').show();
        $('.testi-img-0').addClass('testImg');
        $(imgEnd).removeClass('testImg');
        prevSlide = fulllength;
        mynextSlide = Number(currentSlide) + Number(1);
      }
      var selectorCurrent = '.test-info-'+currentSlide,
          selectorPrev = '.test-info-'+prevSlide,
          selectorNext = '.test-info-'+mynextSlide,
          endselector = '.test-info-'+fulllength,
          imgCurrent = '.testi-img-'+currentSlide,
          imgPrev = '.testi-img-'+prevSlide,
          imgNext = '.testi-img-'+mynextSlide,
          imgEnd = '.testi-img-'+fulllength;
      $(selectorCurrent).fadeIn(750);
      $(imgCurrent).addClass('testImg');
      $(imgPrev).removeClass('testImg');
      $(imgNext).removeClass('testImg');
      $(selectorPrev).hide();
      $(selectorNext).hide();
    });

  /**
   * Кнопка вернутся наверх
   */
  $(window).scroll(function() {
      if ($(this).scrollTop()) {
          $('#toTop').fadeIn();
      } else {
          $('#toTop').fadeOut();
      }
  });
  $("#toTop").click(function() {
      $("html, body").animate({scrollTop: 0}, 1000);
   });
}
