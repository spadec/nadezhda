<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
use Joomla\CMS\Factory;
$db = JFactory::getDbo();
// Create a new query object.
$query = $db->getQuery(true);
$query->select($db->quoteName(array('field_id','item_id', 'value', '#__fields.title')));
$query->from($db->quoteName('#__fields_values'));
$query->join('INNER', $db->quoteName('#__fields') . ' ON ' . $db->quoteName('#__fields_values.field_id') . ' = ' . $db->quoteName('#__fields.id'));
$query->where($db->quoteName('item_id') . ' = '. $db->quote('8'));
$db->setQuery($query);

// Load the results as a list of stdClass objects (see later for more options on retrieving data).
$results = $db->loadObjectList();
$allusers = $results[0];
unset($results[0]);
rsort($results);
$document = Factory::getDocument();
$document->addStyleSheet(JUri::base() ."templates/nadezhda/css/circle.css");
?>
<div class="container">
	<div class="row align-items-center" id="numeral">
	<?php for($i=0;$i<count($results);$i++): ?>
	<?php $values = explode(',',$results[$i]->value); ?>	
		<div class="col-md-3 text-center mt-4">
			<div class="circles-container d-flex justify-content-center">
				<div class="circlebar" data-circle-startTime=0 data-circle-maxValue="<?php echo $values[1]; ?>" data-circle-dialWidth=3 data-circle-size="127px" data-circle-type="progress">
					<div class="loader-bg">
						<div id="circleText" class="text"><?php echo $values[0]; ?></div>
					</div>
				</div>
			</div>
			<div class="text-center">
				<p class="num-note" id="num-note"><?php echo $results[$i]->title; ?></p>
			</div>
		</div>
	<?php endfor; ?>
		<div class="col-md-3 text-center px-0">
			<div class="minimap d-flex justify-content-center align-items-center">
				<?php echo $allusers->value; ?>
			</div>
			<p id="" style="color:#fff"><?php echo $allusers->title; ?></p>
		</div>
	</div>
</div>