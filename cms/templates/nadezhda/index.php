<?php defined( '_JEXEC' ) or die( 'Restricted access' );?>
<?php
	$this->setGenerator('');
	$this->_scripts = array();
	unset($this->_script['text/javascript']);
	$lang = JFactory::getLanguage();
	$lang_tag = $lang->getTag();
	$languages	= JLanguageHelper::getLanguages();
	$lang_index = 0;
	for ($i=0; $i < count($languages); $i++) { 
		if($languages[$i]->lang_code!==$lang_tag){
			$lang_index = $i;
		}
	}
?>
<!DOCTYPE html>
<html dir="ltr" lang="ru-RU">
<head>
	<jdoc:include type="head" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="Content-Security-Policy" content="default-src *;
    img-src * 'self' data: https: http:;
    script-src 'self' 'unsafe-inline' 'unsafe-eval' *;
    style-src  'self' 'unsafe-inline' *">
</head>
<body>
	<div class="container">
		<!-- start Mobile menu-->
		<div class="collapse navbar-collapse row d-lg-none fixed-top" id="mobileMenu">
			<div class="d-flex align-items-center px-3">
				<div class="col-6" id="mobileLogo">
					<img src="/templates/nadezhda/images/menu-logo.png" alt="" class="img-fluid">
				</div>
				<div class="col-2" id="mobileLang">
					<span class="lang-name"><a href="<?php echo $languages[0]->link; ?>">Қазақша</a></span>
				</div>
				<div class="col-4 text-right togglerButton">
					<button class="navbar-toggler" style="display: none;" type="button" id="closeButton">
						<span class="navbar-toggler-icon" id="closeIcon"></span>
					</button>
				</div>
			</div>
			<jdoc:include type="modules" name="mobilemenu" />
			<div class="extmobile px-3">
				<div class="col-9">Техподдержка  <a href="#"><strong>+7(715)239-00-28</strong></a></div>
				<div class="col-9">Продажи  <a href="#"><strong> +7(715)239-09-35</strong></a></div>
			</div>
			<div class="menuCallback px-3">
				<button class="btn button joomly-callback">Обратный звонок</button>
			</div>
		</div>
		<!-- end Mobile menu-->
		<!-- start Top layout-->
		<div class="row d-none d-lg-block " id="topContacts">
			<div class="col-lg-12 d-flex" >
				<div class="col-lg-5">
					Связаться с нами  <a href="mailto:sales@skomed.kz">sales@skomed.kz </a>
				</div>
				<div class="col-lg-7 d-flex text-right">
					<div class="col-lg-6">Техподдержка  <a href="tel:+7715-239-00-28">+7(715)239-00-28</a></div>
					<div class="col-lg-6">Продажи  <a href="tel:+7715-239-09-35"> +7(715)239-09-35</a></div>
				</div>
			</div>
		</div>
		<!-- end Top layout-->
		<!-- start mainmenu -->
		<div class="row align-items-center" id="rowLogoAndMenu">
			<div class="col-lg-3 col-6">
				<div class="col-12" id="logo">
					<a href=""><img src="/templates/nadezhda/images/head-logo.png" alt="" class="img-fluid"></a>
				</div>
			</div>
			<div class="col-2 d-lg-none" id="mobileLang"><span class="lang-name"><a href="<?php if($languages[$lang_index]->link=='/'){echo JUri::base()."ru/"; }else{echo $languages[$lang_index]->link; } ?>"><?php echo $languages[$lang_index]->title_native; ?></a></span></div>
			<div class="col-4 d-lg-none text-right togglerButton">
				<button class="navbar-toggler" type="button" id="tooglerdata">
					<span class="navbar-toggler-icon" id="tooglericon"></span>
				</button>
			</div>
			<div class="col-lg-9 col-7">
				<div class="d-none d-lg-block">
					<div class="row"> 
						<div class="col-6" id="info">График работы: Пн - Пт: 8:00 - 17:00 (13:00 - 14:00 перерыв)</div>
						<div class="col-3" id="langs"><jdoc:include type="modules" name="languages" /></div>
						<div class="col-3" id="callback"><a class="joomly-callback"><img src="/templates/nadezhda/images/icons/Mobile.png" id="mobileIcon" />Обратный звонок</a></div> 
					</div>
				</div>
				<jdoc:include type="modules" name="mainmenu" />
			</div>
		</div>
		<!-- end mainmenu -->
		<!-- start submenu -->
		<jdoc:include type="modules" name="submenu" />
		<!-- end submenu -->
		<!-- start breadcrumbs -->
		<jdoc:include type="modules" name="breadcrumbs" />
		<!-- end breadcrumbs -->
		<!-- start slider-->
		<jdoc:include type="modules" name="slider" />
		<!--end slider-->
		<!-- start blocks-->
		<jdoc:include type="modules" name="blocks" />
		<!-- end blocks-->
		<?php if(JURI::current()!== JURI::base()): ?>
		<jdoc:include type="message" />
		<jdoc:include type="component" />
		<?php endif; ?>
		<!--start About section-->
		<?php if(JURI::current()== JURI::base()): ?>
		<div class="row justify-content-center" id="about">
			<div class="col-lg-7 col-md-6 col-sm-12 align-self-start" id="aboutProgramm">
				<jdoc:include type="modules" name="about" />
			</div>
			<div class="col-lg-5 col-md-6 col-sm-12 align-self-center px-0"  id="submitApp">
				<jdoc:include type="modules" name="sendRequest" />

			</div>
		</div>
		<?php endif; ?>
		<!--end About section-->
		<!--start numeral section-->
		<jdoc:include type="modules" name="numeral" />
		<!--end numeral section-->
		<!--start Why section-->
		<jdoc:include type="modules" name="why" />
		<!--end Why section-->
		<!--start video section-->
		<jdoc:include type="modules" name="video" />
		<!--end video section-->
		<!--start integration section-->
		<jdoc:include type="modules" name="integration" />
		<!--end integration section-->
		<!--start news section-->
		<jdoc:include type="modules" name="news" />
		<!--end news section-->
		<!--start testimonials section-->
		<jdoc:include type="modules" name="testimonials" />
		<!--end testimonials section-->
		<!--start mobile-app section-->
		<jdoc:include type="modules" name="mobileApp" />
		<!--end mobile-app section-->
		<!--start map section-->
		<jdoc:include type="modules" name="map" />
		<!--end map section-->
		<!--start footer section-->
		<div class="row pt-4 pb-4 my-auto" id="footer">
			<div class="col-md-4 my-auto">
				<div class="footer-logo">
					<a href="<?php echo JURI::base(); ?>"><img src="/templates/nadezhda/images/foot-logo.png" class="img-fluid" alt=""></a>
				</div>
				<div class="footer-info mt-2">График работы: Понедельник - Пятница: 8:00 по 17:00 (13:00 по 14:00 перерыв на обед)</div>
			</div>
			<div class="col-md-2 p-0 my-auto d-none d-md-block d-lg-block">
				<jdoc:include type="modules" name="footermenu1" />
			</div>
			<div class="col-md-3 my-auto d-none d-md-block d-lg-block">
				<jdoc:include type="modules" name="footermenu2" />
			</div>
			<div class="col-md-3 my-auto">
				<p class="contact-phone">
					Телефон техподдержки: 
				</p>
				<p class="contanct-phone-text">
					+7(715)239-00-28 (WhatsApp)
				</p>
				<p class="contact-phone">
					Продажи: 
				</p>
				<p class="contanct-phone-text">
					+7(715)239-09-35
				</p>
				<a href="mailto:info@skomed.kz">info@skomed.kz</a>
			</div>
		</div>
		<!--end footer section-->
		<!--start bottom section-->
		<div class="row pt-4 pb-4" id="bottom">
			<div class="col-md-6">
				© ИП "PROFIT" 2020. Всe права защищены
			</div>
			<div class="col-md-3">
				<!-- Yandex.Metrika counter -->
				<script type="text/javascript" crossorigin="anonymous">
				   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
				   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
				   (window, document, "script", "/tag.js", "ym");

				   ym(62708281, "init", {
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true
				   });
				</script>
				<noscript><div><img src="https://mc.yandex.ru/watch/62708281" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
			</div>
			<div class="col-md-3 madeby">
				Сделано в студии  <a href="http://citrus.kz">citrus.kz</a>
			</div>
		</div>
        <!--end bottom section-->
	</div>
		<div id='toTop' class="text-center">
			🠝 
		</div>  
		<script src="/templates/nadezhda/bundle.js"></script>
	</body>
</html>
