<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_menu
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// The menu class is deprecated. Use nav instead
?>
<div class="row d-none" id="submenu">
    <nav class="subnavbar">
        <ul class="subnavbar-nav">
            <div class="closeSubmenu">
                <span class="subnavbar-toggler-icon" id="closeSubIcon"></span>
            </div>
        </ul>
    </nav>
</div>