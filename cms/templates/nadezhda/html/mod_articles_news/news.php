<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="row mx-auto mt-5" id="news">
    <div class="text-center w-100"><h2 id="pageTitle"><?php echo $module->title; ?></h2></div>
        <div class="slider multiple-items">
        <?php foreach ($list as $item) : ?>
            <?php	$images = json_decode($item->images);
                    $image = $images->image_intro;
                    //$created = explode(' ',$item->created);
                    $newFormat = date('d-m-Y',strtotime($item->created)); 	?>
            <div class="padding-none" >
                <div class="card" >
                    <a href="<?php echo $item->link; ?>"><img src="<?php echo $image; ?>" class="news-img" alt="..."></a>
                    <div class="news-date w-20"><?php echo $newFormat; ?></div>
                    <div class="card-body">
                        <h4 class="card-title">	<a href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h4>
                        <p class="card-text"><?php echo strip_tags($item->introtext); ?></p>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>    
        </div>
</div>
