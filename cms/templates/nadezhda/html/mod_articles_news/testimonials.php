<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$i = 0; $j = 0;
?>

<div class="row" id="testimonials">
    <div class="col-lg-12 text-center p-5">
        <h2 id="pageTitle"><?php echo $module->title; ?></h2>
    </div>
    <div class="col-lg-12 testi-items" >
        <?php foreach ($list as $item) : ?>
                <?php	$images = json_decode($item->images);
                        $image = $images->image_intro;
        ?>
            <div class="my-auto customer-photo" data-id="<?php echo $item->id; ?>">
                <a href="<?php echo $item->link; ?>"><img src="<?php echo $image; ?>" alt="" class="testi-img-<?php echo $j; ?> img-fluid mx-auto"></a>
            </div>

        <?php $j++; endforeach; ?>
    </div>
    <?php foreach ($list as $item) : ?>
    <div class="testi-all test-info-<?php echo $i; ?>" style="display: none">
        <div class="text-center p-3">
            <h3><?php echo $item->title; ?></h3>
            <span id="aboutOrg"><i><?php echo strip_tags($item->introtext); ?></i></span>
        </div>
        <div class="row text-center pt-3 pb-5 mx-3">
            <p><?php echo strip_tags($item->fulltext); ?></p>       
        </div>
    </div>
    <?php $i++; endforeach; ?>
</div>