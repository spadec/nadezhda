<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
		<div class="row" id="Why">
			<h2 id="pagetitle"><?php echo $module->title; ?></h2>
			<div class="container">
				<div id="cards" class="row">
					<div class="col-lg-6">
						<?php $countall = count($list);
						if(($countall % 2) == 0){
							$start = $countall/2;
						}
						else{
							$start = intval($countall/2)+1;
						}
						?>
                        <?php for($i=0;$i<=$start-1;$i++): ?>
                            <?php	$images = json_decode($list[$i]->images);
	                                $image = $images->image_intro; 	?>
						<?php
							if($i > 2){
								echo '<div class="spoiler" style="display:none">';
							}
						?>
						<div class="row" id="card">
							<div id="cardNote" class="col-sm-10 my-auto"><?php echo $list[$i]->introtext; ?></div>
							<div id="cardIcon" class="col-sm-2 my-auto text-center"><img src="<?php echo $image; ?>" class="img-fluid"></div>
						</div>
						<?php if($i>2){echo '</div>';}?>
                        <?php endfor; ?>
					</div>
					<div class="col-lg-6">
                        <?php for($i=$start;$i<=$countall-1;$i++): ?>
                            <?php	$images = json_decode($list[$i]->images);
									$image = $images->image_intro; 	?>
						<?php
							if($i > $start+2){
								echo '<div class="spoiler" style="display:none">';
							}
						?>
						<div class="row" id="card">
							<div id="cardNote" class="col-sm-10 my-auto"><?php echo $list[$i]->introtext; ?></div>
							<div id="cardIcon" class="col-sm-2 my-auto text-center"><img src="<?php echo $image; ?>" class="img-fluid"></div>
						</div>
						<?php if($i>$start+2){echo '</div>';}?>
                        <?php endfor; ?>
					</div>
				</div>
			<div class="row ">
				<div class="col-12 text-center" id="buttonAll">
					<button id="spoiler" class="btn button">Посмотреть все</button>
				</div>
			</div>    
			</div>
		</div>