<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');

$dispatcher = JEventDispatcher::getInstance();

$this->category->text = $this->category->description;
$dispatcher->trigger('onContentPrepare', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$this->category->description = $this->category->text;

$results = $dispatcher->trigger('onContentAfterTitle', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayTitle = trim(implode("\n", $results));

$results = $dispatcher->trigger('onContentBeforeDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$beforeDisplayContent = trim(implode("\n", $results));

$results = $dispatcher->trigger('onContentAfterDisplay', array($this->category->extension . '.categories', &$this->category, &$this->params, 0));
$afterDisplayContent = trim(implode("\n", $results));

?>
<div class="page-header"><h2 id="pageTitle"><?php echo $this->escape($this->params->get('page_heading')); ?></h2></div>
<?php $leadingcount = 0; ?>
    <?php if (!empty($this->lead_items)) : ?>

        <div class="row" id="news">
			<?php foreach ($this->lead_items as &$item) : ?>
                    <?php $this->item = &$item; 
                    $images = json_decode($item->images);
                    //print_r($images);
                    $image = $images->image_fulltext;
                    ?>
                <?php $link = JRoute::_(ContentHelperRoute::getArticleRoute($this->item->slug, $this->item->catid, $this->item->language));?>
                <div class="col-md-4 padding-none" >
                    <div class="card" >
                        <a href="<?php echo $link; ?>"><img src="<?php echo $image; ?>" class="card-img-top" alt="..."></a>
                        <div class="card-body">
                            <h5 class="card-title"><a href="<?php echo $link; ?>"><?php echo $item->title; ?></a></h5>
                            <p class="card-text"><?php echo strip_tags($item->introtext); ?></p>
                        </div>
                    </div>
                </div>
                <?php $leadingcount++; ?>
			<?php endforeach; ?>
        </div><!-- end items-leading -->
        <?php echo $afterDisplayContent; ?>
    <?php endif; ?>
    <?php if (($this->params->def('show_pagination', 1) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->get('pages.total') > 1)) : ?>
		<div class="pagination">
			<?php if ($this->params->def('show_pagination_results', 1)) : ?>
				<div class="row w-100">
					<div class="col-sm-2">
					<p class="counter pull-left"> <?php echo $this->pagination->getPagesCounter(); ?> </p>
					</div>
					<div class="col-sm-10">
					<?php echo $this->pagination->getPagesLinks(); ?> </div>
					</div>
				</div>
			<?php endif; ?>
	<?php endif; ?>