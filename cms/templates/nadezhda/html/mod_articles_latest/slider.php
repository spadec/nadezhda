<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$i = 0;
?>
<div class="owl-carousel">
<?php foreach ($list as $item) : ?>
<?php	$images = json_decode($item->images);
	$image = $images->image_intro; 
	$urls = json_decode($item->urls);	?>
	<div class="slider">
		<img src="<?php echo $image; ?>" class="img-fluid" alt="">
		<div class=" col-12 slider-caption">
			<div class="col-lg-6 slide-caption">
				<h1><?php echo $item->title; ?></h1>
					<p><?php echo $images->image_intro_alt; ?></p>
				<div class="slide-button">
				<?php
					if($images->image_intro_caption): ?>
					<button class="btn button <?php echo $urls->urlatext; ?>"><?php echo $images->image_intro_caption; ?>  <span style="font-size: 22px;padding:5px 0 0 10px">→</span></button>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
<?php endforeach; ?>
</div>