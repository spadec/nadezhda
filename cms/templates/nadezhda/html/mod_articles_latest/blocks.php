<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$i = 0;
?>
<div class="row" id="blocks">
<?php foreach ($list as $item) : ?>
<?php	$images = json_decode($item->images);
	$image = $images->image_intro; 	?>
    <a style="text-decoration: none" href="<?php echo $item->link; ?>"><div class="col-md-4 pr-0 b01">
        <div class="block-image" style="background-image: url('<?php echo $image; ?>')">
            <div class="blockNumber">0<?php echo $i+1; ?></div>
            <div class="block-caption">
                <h4><?php echo $item->title; ?></h4>
                <p><?php echo strip_tags($item->introtext); ?></p>
            </div>
        </div></a>
    </div>
    <?php $i++; ?>
<?php endforeach; ?>
</div>