<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>


<div id="cinfo">
    <div class="row"><div class="col-12"><h2>МИС «Надежда»</h2></div></div>
    <div class="row">
        <div class="col-md-4">
            <p><img src="/images/icons/Mobile@2x.png" alt="" /></p>
            <strong>Продажи:</strong><br>
            +7(715)239-09-35<br>
            <div class="cseparator"></div>
            <strong>Телефон техподдержки (24/7):</strong><br>
            +7(715)239-00-28 (WhatsApp)<br>
            <div class="cseparator"></div>
            <strong>Электронные адреса:</strong><br>
                Продажи: sales@skomed.kz<br>
                Директор: a.yusichev@skomed.kz<br>

        </div>
        <div class="col-md-4">
           <p> <img src="/images/icons/LocationPin2x.png" alt="" /></p>
            <strong>Адрес офиса:</strong><br>
            150000, Республика Казахстан, Северо-Казахстанская область, <br>
            г. Петропавловск <br>
            пр. Свердлова 2<br>
        </div>
        <div class="col-md-4">
        <p> <img src="/images/icons/Mails@2x.png" alt="" /></p>
            <strong>Электронный адрес:</strong><br>
            a.yusichev@gmail.com<br>
            <div class="cseparator"></div>
            <strong>График работы:</strong><br>
            Пн - Пт:   08:00 по 17:00 <br>
            перерыв на обед:  13:00 по 14:00<br>
        </div>
    </div>
</div>
<div class="row m-0" >
    <div class="col-lg-5 mr-0" id="cform">
        <form action="" method="post">
            <input type="text" name="cname" id="cname" placeholder="ФИО"/>
            <input type="text" name="cemail" id="cemail" placeholder="email" />
            <textarea name="message" id="message" cols="50" rows="5" placeholder="Сообщение"></textarea>
            <br><button class="btn modal-button " type="submit">Отправить</button>
        </form>
    </div>
    <div class="col-lg-3 mr-0 px-lg-0" id="props">
        <h2>Реквизиты</h2>
        <div class="props-info">
        ИП "PROFIT" <br>
        ИИН 870108350737, 150000, Республика Казахстан, Северо-Казахстанская область, 
        г. Петропавловск ул. Букетова 57<br>
        Cерия и номер Свидетельства: 08915 №0142729 от 17.10.2012<br>
        ИИК KZ9192617015N3170000<br>
        БИК KZKOKZKX<br>
        АО "Казкоммерцбанк"<br>
        Директор Юсичев Антон Петрович<br>
        </div>
    </div>
    <div class="col-lg-4 mr-0" id="props">
        <h2></h2><p></p>
        <div class="props-info">
        ТОО "Антис-Мед"<br>
        БИН 190540027432 КБЕ 17<br>
        Казахстан, область Северо-Казахстанская, город Петропавловск, улица Советская, здание 43, почтовый индекс 150000<br>
        8(747)987-80-77<br>
        a.yusichev@gmail.com <br>
        ИИК (IBAN счет) KZ29722S000002177552<br>
        БИК CASPKZKA<br>
        АО "Kaspi Bank"<br>
        Филиал АО "Kaspi Bank" в г. Петропавловск<br>
        Директор Юсичев Антон Петрович <br>
        </div>
    </div>
</div>
<?php
if(isset($_POST["cemail"])){
    $mailer = JFactory::getMailer();
    $phone = $_POST["cemail"];
    $name = $_POST['cname'];

    $message = "Имя: ".$name."<br />";
    $message .= "Email: ".$phone."<br />";
        $mailer->setSubject('Сообщение со страницы контакты сайта МИС Надежда');
        $mailer->IsHTML( true );
        $mailer->setSender( array( 'info@надежда.kz', 'http://xn--80aalaeg9b.kz/' ) );
        $mailer->addRecipient( 'paul.starkov@gmail.com' );
        $mailer->setBody($message);
        $status = $mailer->send();
            if($status==1){ ?>
                <div class="modal" id="myModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="joomly-callback-closer"><i data-dismiss="modal" id="callback-alert-close<?php if (isset($module->id)){echo $module->id;};?>" class="fa fa-close"></i></div>
                            <div class="p-3">
                                <h3 class="">Сообщение успешно отправленно!</h5>
                            </div>
                            <div class="px-3">
                                <p>Спасибо за обращение. Наши менеджеры свяжутся с Вами в ближайшее время </p>
                            </div>
                            <div class="p-3 text-left">
                                <button type="button" data-dismiss="modal" class="btn modal-button">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
        
        <?php  $app->enqueueMessage(JText::_('Ваше сообщение успешно отправленно'), 'message');
        } 
}