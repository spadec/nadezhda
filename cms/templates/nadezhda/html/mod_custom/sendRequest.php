<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="wrapper mx-auto">
    <h3 id="formTitle"><?php echo $module->title; ?></h3>
    <p>Нужна консультация по автоматизации клиники? Оставьте заявку. Мы перезвоним в течение 10 минут</p>
    <form action="" method="post" name="submitApp">
        <input type="text" required name="name" id="name" placeholder="ФИО" />
        <input type="text" required name="phone" id="phone" placeholder="Номер телефона" />
        <button class="btn">Отправить</button>
    </form>
</div>
<?php
if(isset($_POST["phone"])){
    $mailer = JFactory::getMailer();
    $phone = $_POST["phone"];
    $name = $_POST['name'];

    $message = "Имя: ".$name."<br />";
    $message .= "телефон: ".$phone."<br />";
        $mailer->setSubject('Сообщение модуля обратная связь с сайта МИС Надежда');
        $mailer->IsHTML( true );
        $mailer->setSender( array( 'info@надежда.kz', 'http://xn--80aalaeg9b.kz/' ) );
        $mailer->addRecipient( 'paul.starkov@gmail.com' );
        $mailer->setBody($message);
        $status = $mailer->send();
            if($status==1){ ?>
                <div class="modal" id="myModal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="joomly-callback-closer"><i data-dismiss="modal" id="callback-alert-close<?php if (isset($module->id)){echo $module->id;};?>" class="fa fa-close"></i></div>
                            <div class="p-3">
                                <h3 class="">Сообщение успешно отправленно!</h5>
                            </div>
                            <div class="px-3">
                                <p>Спасибо за обращение. Наши менеджеры свяжутся с Вами в ближайшее время </p>
                            </div>
                            <div class="p-3 text-left">
                                <button type="button" data-dismiss="modal" class="btn modal-button">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
        
        <?php  $app->enqueueMessage(JText::_('Ваше сообщение успешно отправленно'), 'message');
        } 
}
