<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="row" id="map">
    <div class="col-12 p-0" >
        <iframe src="https://www.google.com/maps/d/u/0/embed?mid=1IIEjv60lK9J0fIDRVzXtAFonOZNvRVGp&z=5" width="100%" height="500px" frameborder="0" style="border:0" ></iframe>
    </div>
</div>
