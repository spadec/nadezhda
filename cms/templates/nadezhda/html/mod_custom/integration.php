<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="row" id="integration">
	<div class="col-lg-6 text-center" style="padding-left: 25px">
		<img src="<?php echo $params->get('backgroundimage'); ?>" alt="" class="img-fluid">
	</div>
	<div class="col-lg-6 w-125 mx-auto">
			<h2 id="pageTitle"><?php echo $module->title; ?></h2>
			<?php echo $module->content; ?>
	</div>
</div>
