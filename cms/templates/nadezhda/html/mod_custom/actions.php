<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<div class="row" id="subscribe">
    <div class="background w-100 d-flex">
        <div class="col-md-6 offset-md-3 align-self-center text-center">
            <form action="" method="post">
                <input type="text" name="subscribe" id="subscribe" />
                <button type="submit" class="btn button"><img src="/images/icons/Mail_30px.png" /></button>
            </form>
        </div>
    </div>
</div>