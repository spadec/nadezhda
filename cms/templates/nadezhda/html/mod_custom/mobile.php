<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *
 * @copyright   Copyright (C) 2005 - 2020 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<div class="row" id="mobileApp">
    <div class="col-lg-7 pr-0">
        <h2 class="p-3 pt-5"><?php echo $module->title; ?></h2>
        <?php echo $module->content; ?>
    </div>
    <div class="col-lg-4 text-center">
        <img src="<?php echo $params->get('backgroundimage'); ?>" id="mobileAppImg" alt="" class="img-fluid">
    </div>
</div>
