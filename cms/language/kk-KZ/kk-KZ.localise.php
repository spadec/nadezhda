<?php
defined('_JEXEC') or die;
use \Joomla\String\StringHelper;
abstract class kk_KZLocalise
{
	
	public static function getPluralSuffixes($count)
	{
		if ($count == 0)
		{
			return array('0');
		}
		elseif ($count == 1)
		{
			return array('1');
		}
		else
		{
			return array('MORE');
		}
	}

	public static function getIgnoredSearchWords()
	{
		$search_ignore = array();
		$search_ignore[] = 'href';
		$search_ignore[] = 'lol';
		$search_ignore[] = 'www';
		$search_ignore[] = 'а';
		$search_ignore[] = 'без';
		$search_ignore[] = 'рақмет';
		$search_ignore[] = 'үшін';
		$search_ignore[] = 'жақын';
		$search_ignore[] = 'көбірек';
		$search_ignore[] = 'қарағанда';
		$search_ignore[] = 'үлкен';
		$search_ignore[] = 'үлкенірек';
		$search_ignore[] = 'болады';
		$search_ignore[] = 'болды';
		$search_ignore[] = 'бола тұра';
		$search_ignore[] = 'болды';
		$search_ignore[] = 'сізді';
		$search_ignore[] = 'сізге';
		$search_ignore[] = 'сізбен';
		$search_ignore[] = 'сіздің';
		$search_ignore[] = 'сіздікі';
		$search_ignore[] = 'жақында';
		$search_ignore[] = 'мысалға';
		$search_ignore[] = 'керемет';
		$search_ignore[] = 'ішінде';
		$search_ignore[] = 'іші';
		$search_ignore[] = 'қасында';
		$search_ignore[] = 'міне';
		$search_ignore[] = 'уақыт';
		$search_ignore[] = 'время';
		$search_ignore[] = 'бәрібір';
		$search_ignore[] = 'бар';
		$search_ignore[] = 'әрқашан';
		$search_ignore[] = 'барлығы';
		$search_ignore[] = 'бәрін';
		$search_ignore[] = 'кез-келген';
		$search_ignore[] = 'көрінеді';
		$search_ignore[] = 'қайда';
		$search_ignore[] = 'йә';
		$search_ignore[] = 'алыста';
		$search_ignore[] = 'расында';
		$search_ignore[] = 'күн';
		$search_ignore[] = 'күнде';
		$search_ignore[] = 'үй';
		$search_ignore[] = 'үйде';
		$search_ignore[] = 'үймен';
		$search_ignore[] = 'оны';
		$search_ignore[] = 'егер';
		$search_ignore[] = 'бар';
		$search_ignore[] = 'не';
		$search_ignore[] = 'армандапсың';
		$search_ignore[] = 'біледі';
		$search_ignore[] = 'білді';
		$search_ignore[] = 'білем';
		$search_ignore[] = 'және';
		$search_ignore[] = 'т с.с';
		$search_ignore[] = 'ж т.б';
		$search_ignore[] = 'кейді';
		$search_ignore[] = 'іздеді';
		$search_ignore[] = 'іздеу';
		$search_ignore[] = 'оларға';
		$search_ignore[] = 'ал';
		$search_ignore[] = 'оларды';
		$search_ignore[] = 'жаққа';
		$search_ignore[] = 'әр';
		$search_ignore[] = 'қалай';
		$search_ignore[] = 'болған';
		$search_ignore[] = 'болатын';
		$search_ignore[] = 'кім';
		$search_ignore[] = 'біреу';
		$search_ignore[] = 'қайда';
		$search_ignore[] = 'ең';
		$search_ignore[] = 'кіші';
		$search_ignore[] = 'арасы';
		$search_ignore[] = 'дос';
		$search_ignore[] = 'менікі';
		$search_ignore[] = 'менің';
		$search_ignore[] = 'біз';

		return $search_ignore;
	}

	public static function getLowerLimitSearchWord()
	{
		return 3;
	}

	public static function getUpperLimitSearchWord()
	{
		return 20;
	}

	
	public static function getSearchDisplayedCharactersNumber()
	{
		return 200;
	}

	public static function transliterate($string)
	{
		$str = StringHelper::strtolower($string);

		$glyph_array = array(
			'a' => 'а',
			'a' => 'ә',
			'b' => 'б',
			'v' => 'в',
			'g' => 'ғ',
			'g' => 'г,ғ',
			'd' => 'д',
			'e' => 'е,є,э',
			'jo' => 'ё',
			'zh' => 'ж',
			'z' => 'з',
			'i' => 'и,і',
			'ji' => 'ї',
			'j' => 'й',
			'k' => 'к',
			'k' => 'қ',
			'l' => 'л',
			'm' => 'м',
			'n' => 'н',
			'o' => 'о',
			'o' => 'ө',
			'p' => 'п',
			'r' => 'р',
			's' => 'с',
			't' => 'т',
			'u' => 'ұ',
			'u' => 'ү',
			'u' => 'у',
			'f' => 'ф',
			'kh' => 'х',
			'h' => 'һ',
			'ts' => 'ц',
			'ch' => 'ч',
			'sh' => 'ш',
			'shch' => 'щ',
			'' => 'ъ',
			'y' => 'ы',
			'' => 'ь',
			'yu' => 'ю',
			'ya' => 'я',
		);

		foreach ($glyph_array as $letter => $glyphs) {
			$glyphs = explode(',', $glyphs);
			$str = StringHelper::str_ireplace($glyphs, $letter, $str);
		}

		$str = preg_replace('#\&\#?[a-z0-9]+\;#ismu', '', $str);

		return $str;
	}
}
